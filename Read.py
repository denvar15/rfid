#!/usr/bin/env python
# -*- coding: utf8 -*-
#
#    Copyright 2014,2018 Mario Gomez <mario.gomez@teubi.co>
#
#    This file is part of MFRC522-Python
#    MFRC522-Python is a simple Python implementation for
#    the MFRC522 NFC Card Reader for the Raspberry Pi.
#
#    MFRC522-Python is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    MFRC522-Python is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with MFRC522-Python.  If not, see <http://www.gnu.org/licenses/>.
#

import RPi.GPIO as GPIO
import MFRC522
import signal
import rospy
from std_msgs.msg import Bool
from std_msgs.msg import UInt8MultiArray

continue_reading = True
code = []

def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

def callback(data):
    global pub
    if data:
       File = open('RFID.txt')
       code = File.read()
       print([int(i) for i in code[1:-1].split(", ")])
       data_to_send = UInt8MultiArray(data=[int(i) for i in code[1:-1].split(", ")])  # the data to be sent, initialise the array
       # data_to_send.data = [int(i) for i in code[1:-1].split(", ")]  # assign the array with the value you want to send
       # data_to_send = UInt8MultiArray(data=[255, 2, 3])
       pub.publish(data_to_send)

# Init
# rospy.init_node('rosserial_python')
rospy.init_node('RFID')
rospy.Subscriber('/send', Bool, callback)
pub = rospy.Publisher('/HC12', UInt8MultiArray)

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# Welcome message
print "Welcome to the MFRC522 data read example"
print "Press Ctrl-C to stop."

while continue_reading:
    
    # Scan for cards    
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print "Card detected"
    
    # Get the UID of the card
    (status,uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:

        # Print UID
        print "Card read UID: %s,%s,%s,%s" % (uid[0], uid[1], uid[2], uid[3])
    
        # This is the default key for authentication
        key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

        # Select the scanned tag
        MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 61, key, uid)

        # Check if authenticated
        if status == MIFAREReader.MI_OK:
            code = MIFAREReader.MFRC522_Read(61)
            File = open('RFID.txt', 'w')
            File.write(str(code))
            File.close()
    
        # This is the default key for authentication
        key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

        # Select the scanned tag
        MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 61, key, uid)

        # Check if authenticated
        if status == MIFAREReader.MI_OK:
            code = MIFAREReader.MFRC522_Read(61)
            File = open('RFID.txt', 'w')
            File.write(str(code))
            File.close()
            MIFAREReader.MFRC522_StopCrypto1()
            break
        else:
            print "Authentication error"

while continue_reading:
   pass